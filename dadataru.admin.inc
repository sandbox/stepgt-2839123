<?php

/**
 * @param $form
 * @param $form_state
 * @return mixed
 */
function dadataru_settings_page($form, &$form_state) {
  $form['dadataru_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Dadata API key'),
    '#default_value' => variable_get('dadataru_token', '')
  );

  $form['dadata_jq_plugin'] = array(
    '#type' => 'fieldset',
    '#title' => t('jQuery plugin')
  );

  $form['dadata_jq_plugin']['dadataru_suggestions_external'] = array(
    '#type' => 'radios',
    '#options' => array(
      'cdn.jsdelivr.net' => 'https://cdn.jsdelivr.net',
      'dadata.ru' => 'https://dadata.ru',
      'local' => 'local'
    ),
    '#title' => t('Use external JS/CSS libraries'),
    '#default_value' => variable_get('dadataru_suggestions_external', 'local'),
    '#description' => t('Use latest external JS/CSS library from <span class="dadataru_suggestions_external">https://cdn.jsdelivr.net</span>')
  );

  $form['dadata_jq_plugin']['dadataru_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Dadata API URL'),
    '#default_value' => variable_get('dadataru_api_url', 'https://suggestions.dadata.ru/suggestions/api/4_1/rs'),
    '#description' => t('The URL of service for jQuery plugin. Should be absolute (beginning with a scheme http/https)')
  );

  return system_settings_form($form);
}