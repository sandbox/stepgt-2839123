(function ($) {
    Drupal.behaviors.dadataru = {
        attach: function (context, settings) {
            //
            $('input#edit-test-dd').suggestions({
                serviceUrl: settings.dadataru.dadataru_api_url,
                token: settings.dadataru.dadataru_token,
                type: "NAME",
                triggerSelectOnSpace: true,
                hint: "hint",
                noCache: true,
                onSelect: function (suggestion) {
                    if (console) {
                        console.log(suggestion);
                    }
                }
            });
        }
    };
})(jQuery);